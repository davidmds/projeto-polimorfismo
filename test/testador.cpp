#include <iostream>
#include <fstream>
#include <string>
#include "../circuito.h"
#include "../porta.cpp"
#include "../portas.cpp"
#include "../circuito.cpp"

using namespace std;

int main(){
    Porta_XOR N;

    N.digitar();



    /*
    //Digitando as informa��es e salvando no arquivo
    ofstream arq_o("arquivo.txt");
    N.imprimir(arq_o);
    arq_o.close();

    //Lendo arquivo e imprimindo conte�do
    ifstream arq_i("arquivo.txt");
    arq_i.ignore(3, ' '); // gambiarra
    N.ler(arq_i);
    cout << "Conteudo do arquivo:\n";
    cout << N;
    arq_i.close();
    */

    bool_3S in[3];

    in[0] = TRUE_3S;
    in[1] = FALSE_3S;
    in[2] = FALSE_3S;

    bool_3S res = N.simular(in);

    if(res == TRUE_3S){
        cout << "true" << endl;
    } else if(res == FALSE_3S){
        cout << "false" << endl;
    } else if(res == UNDEF_3S){
        cout << "undef" << endl;
    } else {
        cout << "Erro." << endl;
    }

    cout << N;

    return 0;
}
