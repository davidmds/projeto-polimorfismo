#ifndef _CIRCUITO_CPP_
#define _CIRCUITO_CPP_

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include "circuito.h"

/*
    unsigned Nin;      // Numero de entradas
    unsigned Nout;     // Numero de saidas
    unsigned Nportas;  // Numero de portas
    bool_3S *inputs;   // array com dimensao "Nin"
    int *id_out;       // array com dimensao "Nout"
    ptr_Porta *portas; // array com dimensao "Nportas"
*/

// Private
void Circuito::alocar(unsigned NI, unsigned NO, unsigned NP){
	inputs = new bool_3S[NI];
	for (unsigned i=0; i<NI; i++) inputs[i] = UNDEF_3S;
	id_out = new int[NO];
	for (unsigned i=0; i<NO; i++) id_out[i] = 0;
	portas = new ptr_Porta[NP];
	for (unsigned i=0; i<NP; i++) portas[i] = NULL;

	Nin = NI;
	Nout = NO;
	Nportas = NP;
}
void Circuito::copiar(const Circuito &C){
	alocar(Nin, Nout, Nportas);

	for (unsigned i=0; i<Nin; i++) inputs[i] = C.inputs[i];
	for (unsigned i=0; i<Nout; i++) id_out[i] = C.id_out[i];
	for (unsigned i=0; i<Nportas; i++) portas[i] = C.portas[i];
}
void Circuito::limpar(){
	if (inputs!=NULL){
		delete inputs;
	}
	if (id_out!=NULL){
		delete id_out;
	}
	if (portas!=NULL){
		delete portas;
	}
	Nin = 0;
	Nout = 0;
	Nportas = 0;
	inputs = NULL;
	id_out = NULL;
	portas = NULL;
}

bool Circuito::verificar(void) const{
	// Verificar todas as condições do projeto
    unsigned portaNin;

    if(Nin == 0 || Nout == 0 || Nportas == 0)
        return false;
    for(unsigned i = 0; i < Nportas; i++){
        portaNin = portas[i]->getNumInputs();
        if(portaNin < 1 || portaNin > 4)
            return false;
        for(unsigned j = 0; j < portaNin; j++)
            if(portas[i]->getId_in(j) == 0 || portas[i]->getId_in(j) < -(int)Nin || portas[i]->getId_in(j) > (int)Nportas)
                return false;
    }
    for(unsigned i = 0; i < Nout; i++)
        if(id_out[i] == 0 || id_out[i] < -(int)Nin || id_out[i] > (int)Nportas)
            return false;

	return true;
}
void Circuito::calcularEntradas(unsigned I){
    // I = (id da porta) - 1
    // Procura os valores das entradas da porta[I]
    // Salvar em entradas[]

    unsigned porta_Nin;
    int id_entrada;

    for(unsigned i = 0; i < 4; i++){
        entradas[i] = UNDEF_3S;
    }

    if(I < Nportas){
    	porta_Nin = portas[I]->getNumInputs();
    	for(unsigned i = 0; i < porta_Nin; i++){
    		id_entrada = portas[I]->getId_in(i);
    		if(id_entrada > 0)
    			entradas[i] = portas[id_entrada - 1]->getSaida();
    		else if(id_entrada < 0)
    			entradas[i] = inputs[id_entrada * -1 - 1];
    	}

    } else {
    	cout << "A porta " << I + 1 << " nao existe." << endl;
    }
}

// Public
void Circuito::digitar(){
	string tipo;
	bool flag_loop;
	int int_temp;

	// Limpando os dados anteriores
	limpar();

	cout << "Insira o numero de entradas: ";
	cin >> Nin;
	cout << "Insira o numero de saidas: ";
	cin >> Nout;
	cout << "Insira o numero de portas: ";
	cin >> Nportas;

    // Alocando os ponteiros
	alocar(Nin, Nout, Nportas);

    // Solicitando as informações das portas
    cout << endl << ">> PORTAS:" << endl;
	for(unsigned i = 0; i < Nportas; i++){
		flag_loop = false;

		cout << "PORTA " << i+1 << ": \n";

		do{
			cout << "Insira o tipo da porta (NT, AN, NA, OR, NO, XO ou NX): ";
			cin >> tipo;

			if(tipo == "NT"){
				portas[i] = new Porta_NOT();
			} else if(tipo == "AN"){
				portas[i] = new Porta_AND();
			} else if(tipo == "NA"){
				portas[i] = new Porta_NAND();
			} else if(tipo == "OR"){
				portas[i] = new Porta_OR();
			} else if(tipo == "NO"){
				portas[i] = new Porta_NOR();
			} else if(tipo == "XO"){
				portas[i] = new Porta_XOR();
			} else if(tipo == "NX"){
				portas[i] = new Porta_NXOR();
			} else {
				flag_loop = true;
				cout << "Tipo incorreto." << endl;
			}
		} while(flag_loop);

		portas[i]->digitar();
	}

	// Solicitando as informações das saidas
	cout << endl << ">> SAIDAS:" << endl;
	for(unsigned i = 0; i < Nout; i++){
        do{
        	flag_loop = false;

            cout << "Insira o id da porta que saira na saida " << i+1 << ": ";
            cin >> int_temp;

            if((int)Nin * -1 <= int_temp && int_temp != 0 && int_temp <= (int)Nportas){
				id_out[i] = int_temp;
			} else {
				flag_loop = true;
				cout << "O ID inserido nao existe neste circuito." << endl;
			}
        } while(flag_loop);

	}
}
void Circuito::ler(const char* X){
    string string_temp;
    unsigned int_temp;

    // Limpando dados anteriores
    limpar();

    ifstream arq_i(X);

    getline(arq_i, string_temp, ' ');
    if(string_temp != "CIRCUITO:"){
        cout << "Erro de sintaxe no cabecalho 'CIRCUITO:'." << endl;
        limpar();
        return;
    }
    arq_i >> int_temp;
    if(int_temp > 0){
        Nin = int_temp;
    } else {
    	cout << "O numero de entradas precisa ser maior que 0." << endl;
        limpar();
        return;
    }
    arq_i >> int_temp;
    if(int_temp > 0){
        Nout = int_temp;
    } else {
    	cout << "O numero de saidas precisa ser maior que 0." << endl;
        limpar();
        return;
    }
    arq_i >> int_temp;
    if(int_temp > 0){
        Nportas = int_temp;
    } else {
    	cout << "O numero de portas precisa ser maior que 0." << endl;
        limpar();
        return;
    }
    arq_i.ignore(1, '\n');

    // Alocando com os dados inseridos
    alocar(Nin, Nout, Nportas);

    getline(arq_i, string_temp, '\n');
    if(string_temp != "PORTAS:"){
        cout << "Erro de sintaxe no cabecalho 'PORTAS:'." << endl;
        limpar();
        return;
    }


    for(unsigned i = 0; i < Nportas; i++){
    	arq_i >>  int_temp;
    	if(int_temp != i+1){
    		cout << "ID da entrada " << i+1 << "(em ordem) incorreto." << endl;
	    	limpar();
	    	return;
    	}

    	arq_i.ignore(2, ' ');
    	getline(arq_i, string_temp, ' ');

		if(string_temp == "NT"){
			portas[i] = new Porta_NOT();
		} else if(string_temp == "AN"){
			portas[i] = new Porta_AND();
		} else if(string_temp == "NA"){
			portas[i] = new Porta_NAND();
		} else if(string_temp == "OR"){
			portas[i] = new Porta_OR();
		} else if(string_temp == "NO"){
			portas[i] = new Porta_NOR();
		} else if(string_temp == "XO"){
			portas[i] = new Porta_XOR();
		} else if(string_temp == "NX"){
			portas[i] = new Porta_NXOR();
		} else {
			cout << "Tipo de entrada incorreto." << endl;
			limpar();
			return;
		}
		if(!portas[i]->ler(arq_i)){
            cout << "Nao conseguiu ler a porta " << i+1 << endl;
			limpar();
			return;
		}
    }
    getline(arq_i, string_temp, '\n');
    if(string_temp != "SAIDAS:"){
    	cout << "Erro de sintaxe no cabecalho 'SAIDAS:'." << endl;
    	limpar();
    	return;
    }
    for(unsigned i = 0; i < Nout; i++){
    	arq_i >> int_temp;
    	if(int_temp != i+1){
    		cout << "ID da saida " << i+1 << "(em ordem) incorreto." << endl;
	    	limpar();
	    	return;
    	}
    	arq_i.ignore(2, ' ');
    	arq_i >> id_out[i];
    	if(id_out[i] == 0){
    		cout << "Nao existe porta com ID 0." << endl;
	    	limpar();
	    	return;
    	}
    }

    arq_i.close();

    if(!verificar()){
        limpar();
        return;
    }
}
ostream &Circuito::imprimir(ostream &O) const{
    if(verificar()){
        O << "CIRCUITO: " << Nin << ' ' << Nout << ' ' << Nportas << endl;
        O << "PORTAS:" <<endl;
        for(unsigned i = 0; i < Nportas; i++){
            O << i+1 << ") ";
            portas[i]->imprimir(O);
        }
        O << "SAIDAS:" << endl;
        for(unsigned i = 0; i < Nout; i++){
            O << i+1 << ") " << id_out[i] << endl;
        }
    } else
        O << "Circuito vazio." << endl;

	return O;
}
void Circuito::salvar(const char* X) const{
    ofstream arq_o(X);
    imprimir(arq_o);
    arq_o.close();

    cout << "O arquivo foi salvo com sucesso." << endl;
}
void Circuito::digitarEntradas(){
    int entradaTemp;

    if(Nin > 0){
	    for(unsigned i = 0; i < Nin; i++){
	        do{
	            cout << "Digite a porta " << i+1 << " (1 para TRUE, 0 para FALSE e -1 para UNDEF): ";
	            cin >> entradaTemp;
	        } while( entradaTemp != 1 && entradaTemp != 0 && entradaTemp != -1);
	        switch (entradaTemp){
                case 0:
                    inputs[i] = FALSE_3S;
                    break;
                case 1:
                    inputs[i] = TRUE_3S;
                    break;
                case -1:
                    inputs[i] = UNDEF_3S;
                    break;
	        }
	    }
    } else {
    	cout << "O numero de entradas eh 0." << endl;
    }
}
void Circuito::imprimirEntradas(void) const{
    /*
        Ex:  2 entradas (FALSE_3S e UNDEF_3S)
        ENTRADAS: F X
    */
    cout << "ENTRADAS:";
    for(unsigned i = 0; i < Nin; i++)
        cout << ' ' << inputs[i];
    cout << " -> ";
}
void Circuito::imprimirSaidas(void) const{
    /*
        Ex:  2 saidas (UNDEF_3S e FALSE_3S)
        SAIDAS: X F
    */
    cout << "SAIDAS:";
    for(unsigned i = 0; i < Nout; i++){
        cout << ' ';
    	if(id_out[i] > 0)
    		cout << portas[id_out[i] - 1]->getSaida();
    	else if(id_out[i] < 0)
    		cout << inputs[-(int)id_out[i] - 1];
    }
    cout << endl;
}

void Circuito::simular(){
	bool definiuAlgo, temUndef;
	bool_3S resultado, resultadoAnterior;
    // for entradas
        // Chama calcular entrada

    // Resetando as saídas das portas para limpar simulações anteriores
    for(unsigned i = 0; i < Nportas; i++){
        portas[i]->setSaida(UNDEF_3S);
    }

    //cout << endl;
	do{
		definiuAlgo = temUndef = false;

		for(unsigned i = 0; i < Nportas; i++){
			calcularEntradas(i);

			resultado = portas[i]->simular(entradas);
			resultadoAnterior = portas[i]->getSaida();


            /*cout << "PORTA " << i+1 << ": " << portas[i]->getSaida() << endl;
            cout << "Entradas ";
			for(unsigned k = 0; k < portas[i]->getNumInputs(); k++){
                cout << ' ' << entradas[k];
			}
			cout << endl;
			portas[i]->imprimir(cout);
			cout << "Resultado: " << resultado << endl;*/

			portas[i]->setSaida(resultado);

			//cout << "PORTA " << i+1 << " END: " << portas[i]->getSaida() << endl;



			if(resultado != resultadoAnterior)
				definiuAlgo = true;
			if(resultado == UNDEF_3S)
				temUndef = true;
		}
	} while( definiuAlgo && temUndef);

}
void Circuito::gerarTabela(void){
    unsigned resto, temp;
    // por algum motivo no teste i < pow(3,Nin) do for ele reconhece 27 < 27 = true.
    // colocando pow(3,Nin) em uma variavel resolve o problema
    unsigned total = pow(3,Nin);

    if(Nin > 0){
        for(unsigned i = 0; i < total; i++){
            // Definindo entradas
            temp = i;
            for(unsigned j = Nin; j > 0; j--){
                resto = temp%3;
                temp = temp/3;

                if(resto == 0){
                    inputs[j-1] = FALSE_3S;
                } else if(resto == 1){
                    inputs[j-1] = TRUE_3S;
                } else {
                    inputs[j-1] = UNDEF_3S;
                }
            }

            imprimirEntradas();
            simular();
            imprimirSaidas();
        }
    } else {
        cout << "Impossivel gerar a tabela, pois o numero de entradas eh igual a 0." << endl;
    }
}

#endif // _CIRCUITO_CPP_
