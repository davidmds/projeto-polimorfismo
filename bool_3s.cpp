#ifndef _BOOL_3S_CPP_
#define _BOOL_3S_CPP_

#include <iostream>
#include "circuito.h"

ostream &operator<<(ostream &O, const bool_3S &X){
	if(X == TRUE_3S)
		O << "T";
	else if(X == FALSE_3S)
		O << "F";
	else
		O << "X";
	return O;
}

bool_3S operator!(const bool_3S &X){
	if(X == TRUE_3S)
		return FALSE_3S;
	else if(X == FALSE_3S)
		return TRUE_3S;
	else
		return UNDEF_3S;
}
bool_3S operator&&(const bool_3S &X, const bool_3S &Y){
	if(X == FALSE_3S || Y == FALSE_3S)
		return FALSE_3S;
	else if(X == UNDEF_3S || Y == UNDEF_3S)
		return UNDEF_3S;
	else
		return TRUE_3S;
}
bool_3S operator||(const bool_3S &X, const bool_3S &Y){
	if(X == TRUE_3S || Y == TRUE_3S)
		return TRUE_3S;
	else if(X == UNDEF_3S || Y == UNDEF_3S)
		return UNDEF_3S;
	else
		return FALSE_3S;
}

#endif // _BOOL_3S_CPP_
