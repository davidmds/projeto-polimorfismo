#ifndef _PORTAS_CPP_
#define _PORTAS_CPP_

#include <iostream>
#include <string>
#include "circuito.h"


// PORTA

// Contrutores
Porta::Porta(unsigned NI){
	Nin = NI;
	for(unsigned i = 0; i < Nin; i++)
		id_in[i] = 0;
	saida = UNDEF_3S;
}
Porta::Porta(const Porta &P){
	Nin = P.Nin;
	for(unsigned i = 0; i < Nin; i++)
		id_in[i] = P.id_in[i];
	saida = P.saida;
}

// Definir sa�da
void Porta::setSaida(bool_3S s){
	saida = s;
}

// Retornar id_in[i] caso exista ( existir = 0 < i < Nin )
int Porta::getId_in(unsigned i) const{
	if(i < Nin)
		return id_in[i];
	else
		return 0;
}

//Definir id_in[i] = N se 0 < i < Nin
void Porta::setId_in(unsigned i, int N){
	if(0 < i && i < Nin)
		id_in[i] = N;
}

// Pedir ao usu�rio os dados de uma porta usando cout's e inseri-los na classe
void Porta::digitar(){
	// string tipo;
	// cout << "Insira o tipo da porta (pode ser: NT, AN, NA, OR, NO, XO ou NX): ";
	// cin >> tipo;
	do{
		cout << "Insira a quantidade de entradas (de 2 ate " << NUM_MAX_INPUTS_PORTA << "): ";
		cin >> Nin;
	} while(0 > Nin && Nin > NUM_MAX_INPUTS_PORTA);

	for(unsigned i = 0; i < Nin; i++){
		do{
			cout << "Insira o id da porta que corresponde a entrada " << i+1 << ": ";
			cin >> id_in[i];
		} while(id_in[i] == 0);
	}

}

// Ler da istream I seguindo o
// seguinte padr�o: "Nin: id_in[0] id_in[1] id_in[2] ... id_in[Nin - 1]"
// exemplo: "2: 4 -2"
bool Porta::ler(istream &I){
	I >> Nin;
	if(Nin < 2 && Nin > NUM_MAX_INPUTS_PORTA){
        cout << "Uma porta precisa ter de 2 ate " << NUM_MAX_INPUTS_PORTA << " entradas." << endl;
        return false;
    }
	I.ignore(1,':');
	for(unsigned i = 0; i < Nin; i++){
		I.ignore(1,' ');
		I >> id_in[i];
		if(id_in[i] == 0){
			cout << "Nao existe porta com ID 0." << endl;
			return false;
		}
	}
	I.ignore(1,'\n');

	return true;
}
// Imprimir (inserir em O e retornar O) no mesmo padr�o descrito acima
ostream &Porta::imprimir(ostream &O) const{
	O << Nin << ':';
	for(unsigned i = 0; i < Nin; i++)
		O << ' ' << id_in[i];
	O << endl;

	return O;
}

// PORTAS

// Porta NT
void Porta_NOT::digitar(){
    Nin = 1;
    cout << "Insira o id da entrada: ";
    cin >> id_in[0];
}
bool Porta_NOT::ler(istream &I){
    I >> Nin;
    if(Nin != 1){
        cout << "Uma porta NOT precisa ter apenas 1 entrada." << endl;
        return false;
    }

	I.ignore(1,':');
	for(unsigned i = 0; i < Nin; i++){
		I.ignore(1,' ');
		I >> id_in[i];
		if(id_in[i] == 0){
			cout << "Nao existe porta com ID 0." << endl;
			return false;
		}
	}
	I.ignore(1,'\n');

	return true;
}
ostream &Porta_NOT::imprimir(ostream &O) const{
	O << "NT ";
	Porta::imprimir(O);
}
bool_3S Porta_NOT::simular(const bool_3S in[]){
	return !in[0];
}
// Porta AN
ostream &Porta_AND::imprimir(ostream &O) const{
	O << "AN ";
	Porta::imprimir(O);
}
bool_3S Porta_AND::simular(const bool_3S in[]){
	if(Nin > 1){
		for(unsigned i = 0; i < Nin; i++)
			if(in[i] == FALSE_3S)
				return FALSE_3S;
        for(unsigned i = 0; i < Nin; i++)
			if(in[i] == UNDEF_3S)
				return UNDEF_3S;

		return TRUE_3S;
	}
}
// Porta NA
ostream &Porta_NAND::imprimir(ostream &O) const{
	O << "NA ";
	Porta::imprimir(O);
}
bool_3S Porta_NAND::simular(const bool_3S in[]){
	if(Nin > 1){
		for(unsigned i = 0; i < Nin; i++)
			if(in[i] == FALSE_3S)
				return TRUE_3S;
		for(unsigned i = 0; i < Nin; i++)
            if(in[i] == UNDEF_3S)
				return UNDEF_3S;

		return FALSE_3S;
	}
}
// Porta OR
ostream &Porta_OR::imprimir(ostream &O) const{
	O << "OR ";
	Porta::imprimir(O);
}
bool_3S Porta_OR::simular(const bool_3S in[]){
	if(Nin > 1){
		for(unsigned i = 0; i < Nin; i++)
			if(in[i] == TRUE_3S)
				return TRUE_3S;
        for(unsigned i = 0; i < Nin; i++)
			if(in[i] == UNDEF_3S)
				return UNDEF_3S;

		return FALSE_3S;
	}
}
// Porta NO
ostream &Porta_NOR::imprimir(ostream &O) const{
	O << "NO ";
	Porta::imprimir(O);
}
bool_3S Porta_NOR::simular(const bool_3S in[]){
	if(Nin > 1){
		for(unsigned i = 0; i < Nin; i++)
			if(in[i] == TRUE_3S)
				return FALSE_3S;
        for(unsigned i = 0; i < Nin; i++)
			if(in[i] == UNDEF_3S)
				return UNDEF_3S;

		return TRUE_3S;
	}
}
// Porta XO
ostream &Porta_XOR::imprimir(ostream &O) const{
	O << "XO ";
	Porta::imprimir(O);
}
bool_3S Porta_XOR::simular(const bool_3S in[]){
	unsigned cont_t = 0;

	if(Nin > 1){
		for(unsigned i = 0; i < Nin; i++){
			if(in[i] == UNDEF_3S)
				return UNDEF_3S;
			else if(in[i] == TRUE_3S)
				cont_t++;
		}

		if(cont_t == 1)
			return TRUE_3S;
		else
			return FALSE_3S;
	}
}
// Porta NX
ostream &Porta_NXOR::imprimir(ostream &O) const{
	O << "NX ";
	Porta::imprimir(O);
}
bool_3S Porta_NXOR::simular(const bool_3S in[]){
	unsigned cont_t = 0;

	if(Nin > 1){
		for(unsigned i = 0; i < Nin; i++){
			if(in[i] == UNDEF_3S)
				return UNDEF_3S;
			else if(in[i] == TRUE_3S)
				cont_t++;
		}

		if(cont_t == 1)
			return FALSE_3S;
		else
			return TRUE_3S;
	}
}

#endif // _PORTAS_CPP_
